; #	Win		! Alt		^ Ctrl		+ Shift
#SingleInstance, force 
#NoEnv

;------------- Script management -------------
!F1::
Pause::
    Suspend
    Pause,,1
return
^+F1:: Reload

; ---------------------------------
; Ctrl & Shift & key
; ---------------------------------
^+k::Run calc.exe
;^+h::Run "C:\Users\%A_Username%\OneDrive\MySync"
;^+j::Run "C:\Users\%A_UserName%\OneDrive\MySync\scripts\work\panic_button.ahk"
;^+m::Run "M:\"
;^+o::Run "C:\Users\%A_Username%\OneDrive"
^+x::DllCall("PowrProf\SetSuspendState", "int", 0, "int", 0, "int", 0)
^+1::Run "C:\Users\%A_UserName%\OneDrive\MySync\doc\Budzet-2021.xlsx"
^+2::Run "C:\Users\%A_UserName%\OneDrive\MySync\doc\bilans.xlsx"

^+F12::Send, {Volume_Up}
^+F11::Send, {Volume_Down}
^+F10::Send, {Volume_Mute}

; ---------------------------------
; Ctrl & Alt & key
; ---------------------------------

^!f:: 
    Send, ^c
    Sleep 50
    Run, http://www.google.com/search?q=%clipboard%
    Sleep 50
Return

;----------------------------------
; Other
;----------------------------------

#z:: Send, #{l} ;lock PC

^Numpad0::Send, !{Tab}

^Numpad1::Send, ^c
^Numpad2::Send, ^v
>^>+Numpad2::
    OriginalClipboardContents = %ClipBoardAll%
    ClipBoard = %ClipBoard% ; Convert to text
    Send, ^v 						
    Sleep 100 ; Don't change clipboard while it is pasted
    ClipBoard := OriginalClipboardContents ; Restore original clipboard contents
    OriginalClipboardContents = ; Free memory
Return 	
^Numpad3::Send, ^x

#IfWinActive ahk_exe vlc.exe
    Numpad0::Send, {Space}
    Numpad1::Send, +{Left}
    Numpad2::Send, +{Right}
    Numpad4::Send, ^{l}
    MButton::Send, {Space} 
#IfWinActive

#IfWinActive ahk_exe mintty.exe
    ; **general
    ::eqe::exit
    ;**git-specific
    ::gai::git add -i
    ::ga.::git add .
    ::gap::git add -p
    ::gb::git branch
    ::gc::git commit
    ::gcae::git commit --allow-empty
    ::gch::git checkout
    ::gd::git diff
    ::gdc::git diff --cached
    ::gdt::git difftool
    ::gg::git 
    ::gl::git log -n 
    ::glo::git log --oneline 
    ::gmnff::git merge --no-ff
    ::gpom::git push origin master
    ::gr::git rebase -i head
    ::grh::git reset HEAD~1
    ::gs::git status
    ::gst::git stash
    ::-h::--help
#IfWinActive

#IfWinActive ahk_exe Lightroom.exe
    Numpad1::Send, {w}
    Numpad2::Send, ^{u}
#IfWinActive

#IfWinActive, ahk_exe SumatraPDF.exe
    ^/:: Send, ^{g}
#IfWinActive

#IfWinActive, ahk_exe firefox.exe
^Up::
    WinGetActiveTitle, tab_title
    If(InStr(tab_title, "YouTube"))
    {
        Send, +.
        Return,
    }
    Send, {Up}
Return,
^Down::
    WinGetActiveTitle, tab_title
    If(InStr(tab_title, "YouTube"))
    {
        Send, +,
        Return,
    }
    Send, {Down}
Return,
^Left::
    WinGetActiveTitle, tab_title
    If(InStr(tab_title, "YouTube"))
    {
        Send, j
        Return,
    }
    Send, ^{Left}
Return,
^Right::
    WinGetActiveTitle, tab_title
    If(InStr(tab_title, "YouTube"))
    {
        Send, l
        Return,
    }
    Send, ^{Right}
Return,
; +Left::
;     WinGetActiveTitle, tab_title
;     If(InStr(tab_title, "YouTube"))
;     {
;         Send, ^Left
;         Return,
;     }
;     Send, {+Left}
; Return,
; +Right::
;     WinGetActiveTitle, tab_title
;     If(InStr(tab_title, "YouTube"))
;     {
;         Send, {Ctrl}{Right}
;         Return,
;     }
;     Send, {+Right}
; Return,
#IfWinActive

#IfWinActive, ahk_exe Teams.exe
    MButton::Send, ^+m
#IfWinActive